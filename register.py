#!/usr/bin/env python
"""Register callbacks."""
import random
import connection
import thread

def callback(obj, evtype, function, arguments=None, before=True):
    """Create a callback whenever 'evtype' occurs on 'obj'.

    obj is a Chesterfield object.
    evtype is a PostgreSQL trigger event type. Examples, 'UPDATE', 'INSERT', 'DELETE'.
    function is a Python function to execute when the callback occurs.
    arguments are the arguments to function.
    before is optional, defines if the callback should happen before the event or after it. """
    # This doesn't work!
    cur = connection.con.cursor()
    listen_name = str(obj.__class__.__name__) + "_" + str(random.getrandbits(128))
    cur.execute("CREATE TRIGGER %s %s %s ON %s " % (listen_name,
                                                    (before and "BEFORE") or "AFTER"),
                                                    evtype,
                                                    'objects."' + obj.__class__.__name__ + '"'
                )
    cur.execute("LISTEN %s" % listen_name )

    print "Waiting for 'NOTIFY %s'" % listen_name

    # This is where the doom is!
    while 1:
        if select.select([cur],[],[],5)==([],[],[]):
            print "Timeout"
        else:
            if curs.isready():
                print "Got NOTIFY: %s" % str(curs.connection.notifies.pop())
