#!/usr/bin/env python
import chesterfield
chesterfield.debug.sql = True
DatabaseObject = chesterfield.Chesterfield(database="db_name", user="db_user").object

class Location(DatabaseObject):
    name = 'New Room'
    description = 'A simple room.'

Location(name='nowhere', description='A great absense of space.')

class Thing(DatabaseObject):
    name = 'Nameless Thing'
    location = Location(name='nowhere')

Thing(name = 'Spork', location = Location(name='The Kitchen', description='The Kitchen is full of cupboards and stainless steel appliances.'))
