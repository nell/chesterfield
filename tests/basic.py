#!/usr/bin/env python
import chesterfield
DatabaseObject = chesterfield.Chesterfield(database="db_name", user="db_user").object

class RootObject(DatabaseObject):
    name = 'BaseObject'
    owner = 'wizard'
    permission = 0

class GenericObject(RootObject):
    """Generic located object."""
    location = 'here'

if __name__ == '__main__':
    a = GenericObject(1)[0]
    a.location = 'here'
    
    b = GenericObject()[0]
    b.location = 'spain'
    b.drop()

    print b
