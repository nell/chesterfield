#!/usr/bin/python
"""Chesterfield is an object relational mapping library for storing python objects in a postgresql database.

It is licensed under the GNU General Public License version 2.0."""
import objects
__all__ = ["classes"]
Chesterfield = objects.Chesterfield
