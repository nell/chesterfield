#!/usr/bin/env python
import psycopg2.extensions
sql = False

class Cursor(psycopg2.extensions.cursor):
    def execute(self, sql, args=None):
        if sql:
            print self.mogrify(sql, args)
        psycopg2.extensions.cursor.execute(self, sql, args)
