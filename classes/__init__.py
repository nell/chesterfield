#!/usr/bin/env python
"""Precreated Python types to interface with PostgreSQL types more neatly."""
__all__ = "ipv4"
import ipv4
IPv4Addr = ipv4.IPv4Addr
IPv4AddrTypeError = ipv4.IPv4AddrTypeError
