#!/usr/bin/env python
"""Implements python object <-> PostgreSQL table relationship."""
import traceback, inspect, types
import psycopg2
import classes
import debug

class DatabaseObjectParent(object):
	"""Stub parent object for DatabaseObject used for checking object lineage."""
	pass

class Unimplemented(Exception):
	"""Feature is intentionally unimplemented."""
	pass

class ChesterfieldException(Exception):
	pass

class ClassOutOfScope(ChesterfieldException):
	"""A required class is defined outside of the Chesterfield scope (not kept in the database)."""
	pass

def getdbattribute(self, attr):
	"""Extract attributes from the database.

	Search through the database for an object with the id 'self.id', then find the column name 'attr' and return that value."""
	# Attributes to pull from Python instead of PostgreSQL
	if attr in ("__dict__", "__members__", "__methods__", "__class__",
				"__len__", "__iter__", "__getitem__", "next", "drop"):
		return object.__getattribute__(self, attr)
	cur = object.__getattribute__(self, "con").cursor(cursor_factory=debug.Cursor)
	try:
		cur.execute("""SELECT %s FROM objects."%s" WHERE id = %s""" % (attr, object.__getattribute__(self, "__class__").__name__, object.__getattribute__(self, "id")))
		if cur.rowcount is 1:
			ret = cur.fetchone()[0]
			cur.connection.commit()
			return ret
		else:
			raise psycopg2.ProgrammingError("No results returned by query for attribute.")
	except psycopg2.ProgrammingError:
		cur.connection.rollback()
		traceback.print_exc()
		raise AttributeError("%s instance has no attribute '%s'" % (object.__getattribute__(self, "__class__").__name__, attr))

def typemap(attr):
	"""Map Python type names to PostgreSQL ones.

	Each Python type has to be converted into a PostgreSQL type. Luckily, the conversion is best done automatically by Python itself and Psycopg2. All that is needed is someone to tell Psycopg2 what PostgreSQL type should be used in each case."""
	if isinstance(attr, DatabaseObjectParent):
		raise Unimplemented("Inherited object cross-references are not yet implemented by PostgreSQL.")
	return {str:'text', int:'integer', bool:'boolean', float:'double precision', classes.IPv4Addr:'cidr'}[type(attr)]

class Chesterfield(object):
	def __init__(self, **config):
		if not config.has_key("database"):
			# Set some default configuration
			config["database"] = "chesterfield"
		self.con = psycopg2.connect(**config)
		cur = self.con.cursor(cursor_factory=debug.Cursor)
		# Assure that the 'metadata' and 'objects' schemas are created
		cur.execute("""SELECT nspname FROM pg_namespace WHERE nspname = 'metadata' or nspname = 'objects';""")
		if cur.rowcount is not 2:
			cur.connection.commit()
			# Create the database or exit loudly if we can't
			self.create_db(cur)

	def create_db(self, cur):
		"""Generate the base schema and tables required for storing objects by populating an empty db."""
		# This might be worth expanding to repair some common situations
		# (like one of the two schemas missing)
		try:
			cur.execute("""CREATE SCHEMA metadata;""")
			cur.execute("""CREATE SEQUENCE metadata.classes_id_seq;""")
			cur.execute("""CREATE TABLE metadata.classes (id int DEFAULT nextval('metadata.classes_id_seq') PRIMARY KEY, name text UNIQUE NOT NULL, code text NOT NULL)""")
			cur.execute("""CREATE SEQUENCE metadata.objects_id_seq;""")
			cur.execute("""CREATE TABLE metadata."objects" (id int DEFAULT nextval('metadata.objects_id_seq') PRIMARY KEY)""")
			cur.execute("""CREATE SCHEMA objects;""")
			cur.connection.commit()
		except psycopg2.ProgrammingError:
			cur.connection.rollback()
			traceback.print_exc()
			raise psycopg2.ProgrammingError("Database creation failed, see above traceback for why. This most commonly occurs with partially populated databases.")

	def load_attr(self, name, namespace):
		"""Load the module name using load_class, and attach it to the namespace provided."""
		try:
			exec self.load_class(name=name) in namespace
		except NameError, exc:
			traceback.print_exc()
			print "Name Error, loading: " + str(exc).split("'")[1]
			self.load_attr(str(exc).split("'")[1])

	def load_class(self, **args):
		"""Load class source code from the database."""
		cur = self.con.cursor(cursor_factory=debug.Cursor)
		cur.execute("""SELECT code FROM metadata.classes WHERE name=%(name)s;""", args)
		result = cur.fetchone()
		if result:
			cur.connection.commit()
			return compile(result[0], args['name'], "exec")
		else:
			cur.connection.rollback()
			raise ClassOutOfScope("Class %s is undefined." % args["name"])

	@property
	def classes(self):
		"""A list of all classes defined by Chesterfield."""
		mod = types.ModuleType("Chesterfield.classes", "A dynamic list of Chesterfield classes.")
		cur = self.con.cursor(cursor_factory=debug.Cursor)
		mod.DatabaseObject = self.object
		cur.execute("""SELECT name FROM metadata.classes;""")
		results = cur.fetchall()
		# This is a list comprehension because a regular for each loop
		# doesn't allow for the if clause.
		[self.load_attr(each[0], mod.__dict__) for each in results if each[0] != "DatabaseObject"]
		cur.connection.commit()
		del(mod.__dict__['__builtins__'])
		return mod

	@property
	def object(self):
		"""Construct a Chesterfield root object.
		
		This method accepts the configuration for a new Chesterfield base class."""
		con = self.con
		class DatabaseObject(DatabaseObjectParent):
			"""All persistent objects inherit from here, this is the base database class.

			This is the primary method of using chesterfield. Simply create classes that inherit from this class, and the classes will be reflected in the database."""
			class __metaclass__(type):
				"""Construct classes from class definitions that adhere to the Chesterfield object model.

				This is black magic, but the kind that can be understood by reading the books of Python wizardry. A metaclass is simply a class that instantiates as a class, instead of an object. If you think changing the metaclass is what you need to do, it's probably a good idea to read into the idea long before making any major changes here."""
				def __init__(cls, name, bases, dct):
					"""Construct a full Chesterfield class from the Python class information via introspection."""
					# Verify the database is initialized
					cur = con.cursor(cursor_factory=debug.Cursor)
					# Remove class attributes that shouldn't be stored in the db
					store_dict = dct.copy()
					for each in ('__new__', '__repr__', '__getattribute__', '__setattr__', '__delattr__', '__del__', '__len__', '__getitem__', '__iter__', '__contains__', 'next', 'drop', '__module__', '__metaclass__'):
						if store_dict.has_key(each):
							store_dict.pop(each)
					# Determine the class id from name, assuring the class exists
					cur.execute("""SELECT id FROM metadata.classes WHERE name=%(name)s""", {'name':cls.__name__})
					if cur.rowcount is not 1:
						source = inspect.getsource(cls)
						# Re-indent the source stored, if defined within another structure.
						source = "\n".join([x[source.index(source.splitlines()[0].strip()):] for x in source.splitlines()])
						cur.execute("""INSERT INTO metadata.classes (name, code) VALUES (%(name)s, %(code)s)""", {"name":name, "code":source})
						# Create the instance table, mapping types from python to postgresql
						columns = ', '.join([' '.join((x, typemap(y))) for (x, y) in store_dict.iteritems() if not isinstance(y, classmethod)])
						parents = (bases[0] is not DatabaseObjectParent) and 'objects."' + '", objects."'.join([x.__name__ for x in bases]) + '"' or 'metadata."objects"'
						cur.execute("""CREATE TABLE objects."%s" ( %s ) INHERITS ( %s );""" % (name, columns, parents), store_dict)
						cur.connection.commit()
					# The class exists here
					cur.close()

					# Set class attributes
					cls.__slots__ = store_dict.keys()
					cls.con = con # For database cursor creation later on

					cls.__deletion__ = False
					cls.__cursor__ = None

					type.__init__(cls, name, bases, dct)

			def __new__(typ, *args, **kwargs):
				"""Construct a each Chesterfield object. Takes an argument for an id to retrieve from the existing object tree.

				<class>(<id>) or <class>(<attribute>=<value>,[<attribute>=<value>...])

				Without an id, or with an unused id, this creates a new object in the database. Otherwise it creates a Python reference to the database object.

				An id of -1 searches for objects of this type (and child types).
				No id or 0 creates a new object.

				<class>(-1, <attribute>=<value>) syntax searches for any objects where <attribute> is equal to <value>.
				<class>(<attribute>=<value>) syntax searches for any objects where <attribute> is equal to <value>. If nothing is found, this creates a new object.

				There is one special attribute name, __where__ will be used as an SQL string for the WHERE clause if present. This functionality is a hack, and may be removed in future versions."""
				obj = object.__new__(typ)
				cur = typ.con.cursor(cursor_factory=debug.Cursor)
				if len(args) >= 1 and args[0] is -1:
					# -1, search only
					if len(kwargs) == 0:
						# grab all objects of a type
						cur.execute("""SELECT id FROM objects."%s";""" % (typ.__name__))
					elif "__where__" in kwargs:
						# grab objects of this type that meet this where clause
						# This sucks, I'd like to see a more elegant way of passing an arbitrary WHERE clause
						# It would be neat if someone could figure out how to accept the Python syntax,
						# <Class>(<column1>=="<value>" or <column2>=="<value>"); to produce the WHERE clause,
						# WHERE <column1>='<value>' OR <column2>='<value>'
						cur.execute("""SELECT id FROM objects."%s" WHERE %s;""" % (typ.__name, kwargs["__where__"]))
					else:
						# Drill down using kwargs
						cur.execute("""SELECT id FROM objects."%s" WHERE %s;""" % (typ.__name__, " AND ".join([x[0] + " = %(" + x[0] + ")s" for x in kwargs.items()])), kwargs)
					if cur.rowcount == 0:
						# Nothing found, return an empty object
						object.__getattribute__(obj, "__dict__")["id"] = -1
						object.__getattribute__(obj, "__dict__")["__cursor__"] = cur
						return obj
				elif len(args) >= 1:
					# Select one object, based on the id primary key
					cur.execute("""SELECT id FROM objects."%s" WHERE id = %s;""" % (typ.__name__, args[0]))
				elif len(kwargs) > 0:
					# Select all objects matching the specified attributes
					cur.execute("""SELECT id FROM objects."%s" WHERE %s;""" % (typ.__name__, " AND ".join([x[0] + " = %(" + x[0] + ")s" for x in kwargs.items()])), kwargs)

				# Populate obj.__cursor__ with the set of objects we've selected.
				object.__getattribute__(obj, "__dict__")["__cursor__"] = cur

				# After a set of SQL objects is selected, create the matching python objects
				if (len(args) >= 1 or len(kwargs) > 0) and cur.rowcount > 0:
					# Some set of objects was found
					object.__getattribute__(obj, "__dict__")["id"] = int(cur.fetchone()[0])
				else:
					# The id selected doesn't match anything, create a new object database side and return the python object
					if len(kwargs) > 0:
						cur.execute("""INSERT INTO objects."%s" (%s) VALUES (%s);""" % (typ.__name__, ", ".join(kwargs.keys()), "%(" + ")s, %(".join(kwargs.keys()) + ")s" ), kwargs)
					else:
						cur.execute("""INSERT INTO objects."%s" DEFAULT VALUES;""" % typ.__name__)
					cur.execute("SELECT currval('metadata.objects_id_seq');")
					object.__getattribute__(obj, "__dict__")["id"] = int(cur.fetchone()[0])
				cur.connection.commit()
				return obj

			def __repr__(self):
				"""String representation of chesterfield objects

				Returns strings in the format of:
				'<class name>(<attribute 1>=<non-default value>, <attribute 3>=<non-default value>)'"""
				ret = object.__getattribute__(self, "__class__").__name__
				ret += "("
				ret += "', ".join(["='".join((n, str(y))) for (n, y) in [(x, getdbattribute(self, x)) for x in object.__getattribute__(self, "__slots__")] if y is not None]) + "'"
				ret += ")"
				return ret

			def __getattribute__(self, attr):
				"""Retrieve an attribute from the database, following inheritance rules."""
				# getdbattribute() fetches the raw column value, this method should use getdbattribute to follow
				# the inheritance tree correctly for unset values.
				ret = getdbattribute(self, attr)
				if ret is None:
					# This was the code for retrieving an attribute's value (if null) from the table defaults.
					# This was replaced by doing Python lookups for the same information.
					#cur.execute("""SELECT column_default FROM information_schema.columns WHERE table_name='%s' AND table_schema='objects' AND column_name='%s'""" % (object.__getattribute__(self, "__class__").__name__, attr))
					return getattr(object.__getattribute__(self, "__class__"), attr)
				return ret

			def __setattr__(self, attr, value):
				"""Update attributes in the database."""
				cur = object.__getattribute__(self, "con").cursor(cursor_factory=debug.Cursor)
				try:
					cur.execute("""UPDATE objects."%s" SET %s = '%s' WHERE id = %s""" % (object.__getattribute__(self, "__class__").__name__, attr, value, object.__getattribute__(self, "id")))
					if cur.rowcount is 1:
						object.__getattribute__(self, "con").commit()
					else:
						object.__getattribute__(self, "con").rollback()
						raise psycopg2.ProgrammingError("Attribute does not exist.")
				except psycopg2.ProgrammingError:
					traceback.print_exc()
					raise AttributeError("%s instance has no attribute '%s'" % (object.__getattribute__(self, "__class__").__name__, attr))

			def __delattr__(self, attr):
				"""Reset an attribute to null."""
				self.__setattr__(attr, None)

			def __del__(self):
				"""Object no longer referenced, attempt to drop it if it's up for recycling."""
				cur = object.__getattribute__(self, "con").cursor(cursor_factory=debug.Cursor)
				if object.__getattribute__(self, "__deletion__") is True:
					cur.execute("""DELETE FROM objects."%s" WHERE id = %s""" % (object.__getattribute__(self, "__class__").__name__, object.__getattribute__(self, "id")))
					object.__getattribute__(self, "con").commit()
				cur.close()

			def __len__(self):
				"""Number of rows in the cursor returned by this object."""
				return object.__getattribute__(self, "__cursor__").rowcount

			def __getitem__(self, key):
				"""Return an index from the cursor."""
				cur = object.__getattribute__(self, "__cursor__")
				cur.scroll(key, 'absolute')
				return object.__getattribute__(self, "__class__")(cur.fetchone()[0])

			def __iter__(self):
				"""Cursor iterator for chesterfield objects.

				self.next is a generator that iterates over the cursor self.__cursor__."""
				return self.next()

			def __contains__(self, item):
				"""Searches a cursor for an item."""
				raise Unimplemented("__contains__ needs to be written.")

			def next(self):
				"""Cursor generator for chesterfield objects.

				self.__cursor__ is populated by class.__new__ at object instantiation time."""
				cur = object.__getattribute__(self, "__cursor__")
				index = 0
				while index < cur.rowcount:
					yield self.__getitem__(index)
					index += 1
				raise StopIteration

			def drop(self):
				"""Schedule an object for deletion when the reference count, and listen count are zero."""
				object.__getattribute__(self, "__dict__")["__deletion__"] = True

		# Return the constructed base class
		return DatabaseObject
