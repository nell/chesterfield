This was a class project at OSU. It works reasonably well for most simple classes, but functions in particular were never stored database side.

The original idea was to create a wrapper around PostgreSQL to allow the creation of persistent objects as the framework for an object oriented MUD. This code is abandoned but still used by a few pieces of software (Fishbot being the most useful), so it exists here.

It depends on psycopg2 and a Python version 2.5.* - 2.7.*